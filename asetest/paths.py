import os
from pathlib import Path


class DataFiles:
    datapath = (Path(__file__).parent / 'datafiles').resolve()

    def __init__(self):
        self.paths = self._initialize_paths()

    @classmethod
    def _initialize_paths(cls):
        datapath = cls.datapath
        dct = {}
        dct['abinit'] = [datapath / 'abinit' / name
                         for name in ['LDA_FHI', 'GGA_FHI', 'LDA_PAW']]
        dct['dftb'] = [datapath / 'dftb']
        dct['elk'] = [datapath / 'elk']
        dct['espresso'] = [datapath / 'espresso' / 'gbrv-lda-espresso']
        dct['lammps'] = [datapath / 'lammps']
        dct['openmx'] = [datapath / 'openmx/DFT_DATA19']
        dct['siesta'] = [datapath / 'siesta']
        return dct

    def info(self):
        lines = []
        for name, paths in self.paths.items():
            lines.append(name)
            for path in paths:
                lines.append('    ' + str(path))
        return '\n'.join(lines)

    def __repr__(self):
        return 'DataFiles({})'.format(self.paths)
